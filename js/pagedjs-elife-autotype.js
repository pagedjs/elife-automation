// a plugin to try and fix automatic image placement
// this one will remove the image and add it on the next page.
// and when it keep them.
// at the end of the generation, it fixes the pages with images.
//
// image have caption. should we keep images and caption together?

// image placement
class imgPlacement extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.pushToNextPage = [];
  }

  // before parsed put the fig number as a span in the title for each figure (if it has h2/h3/h4)
  beforeParsed(content) {
    // move the caption around the image
    reorderfigures(content);
    tagFigure(content);
  }

  async afterParsed(content) {
    // add the width and height to the image as attribute to get more data about what they are
    await addWidthHeightToImg(content);
    await calculateDesiredWidthPercentages(
      content.querySelectorAll("figure img")
    );
    // get data-width based on biggest image, and set a data-desired-width.
    // used to get the % in the width
  }

  async finalizePage(pageFragment, oldpage) {
    // when the page is finished, move the element that was on top of the page to the bottom
    if (oldpage.element.querySelector(`.moveToBottom`)) {
      // to avoid commenting things
      const dont = true;
      if (dont) return;

      oldpage.element
        .querySelector(".pagedjs_page_content")
        .lastElementChild.insertAdjacentElement(
          "beforeend",
          oldpage.element.querySelector(".moveToBottom")
        );
    }
  }

  // i don’t use on pageLayout because i’m adding the page as soon as an element is found
  onPageLayout(div, breaktoken, layout) {
    // when trying to add on a page
    for (const thing of this.pushToNextPage) {
      thing.querySelector("img").style.marginTop = "17px";
      thing.querySelector("img").style.marginBottom = "17px";
      thing.classList.add("moveToBottom");
      div.insertAdjacentElement("afterbegin", this.pushToNextPage[0]);
      this.pushToNextPage.shift();
    }
  }
  //
  renderNode(node, templateNode) {
    // if the node is not an elemen do nothing
    if (node.nodeType != 1) return;

    // check if the element can go on the page, or push it to the next one
    // if the next element is a figure
    if (
      templateNode &&
      templateNode.tagName == "FIGURE" &&
      !node.classList.contains("checked")
    ) {
      // get remaining space on the page before the element is added
      let whiteSpace = getRemainingSpace(node);

      // check the height of the image of the element based on the remaining height and the width of the page
      let imageToCheck = templateNode.querySelector("img");

      let height, width;

      if (imageToCheck.dataset.desiredWidth) {
        height =
          imageToCheck.getAttribute("height") *
          Math.round(imageToCheck.dataset.desiredWith);
        width =
          imageToCheck.getAttribute("width") *
          Math.round(imageToCheck.dataset.desiredWith);
      } else {
        width = imageToCheck.getAttribute("width");
        height = imageToCheck.getAttribute("height");
      }

      let resizedHeight = getHeight(width, height, whiteSpace.maxWidth, 17);

      // resize the image the image to the baseline height
      let ratio = checkImageHeightRatio(
        resizedHeight,
        whiteSpace.availableHeight
      );

      // create a new page
      const newPage = this.chunker.addPage();

      // add image receiver to this page to have the first page that has image on it, it will be remove if the page is filled.
      
      if(!previousPage.classList.contains("imageReceiver")) {
      newPage.element.classList.add("imageReceiver");
      }

      // change teh name to use the @page system of paged.js
      // give a name to the page.
      // find it there is enough room to add your thing. if not create a new one.
      // problem is you don’t really know the height of the next thing.
      // so you need to display it, in a page that doesnt exist.
      let contentspace = newPage.element.querySelector(".pagedjs_page_content");

      // insert the element to the newest page to find out its size before trying to put it to the previous one
      contentspace.insertAdjacentElement(
        "afterbegin",
        templateNode.cloneNode(true)
      );

      node.style.display = "none";
      templateNode.style.display = "none";
      templateNode.classList.add("checked");
      //

      // now check if the last page and the previous one are image reciever, and if they can be together move the second bit, and remove the latest page
      //
      const lastpage = getLastPage();
      if (lastpage.classList.contains("imageReceiver")) {
        if (
          lastpage.previousElementSibling?.classList.contains("imageReceiver")
        ) {
          //check if the last page has some room for your newest block
          console.log(
            getRemainingSpaceOnPage(
              lastpage.previousElementSibling.querySelector(
                ".pagedjs_page_content"
              )
            )
          );

          // console.log(lastpage.querySelector("figure"))
          // check if the previous page has enough room for the latest block
          if (
            getRemainingSpaceOnPage(
              lastpage.previousElementSibling.querySelector(
                ".pagedjs_page_content"
              )
            ) > lastpage.querySelector("figure").offsetHeight
          ) {
            //if so, move the newest image in the previous page
            lastpage.previousElementSibling
              .querySelector(".pagedjs_page_content")
              .insertAdjacentElement(
                "beforeend",
                lastpage.querySelector("figure")
              );

            // and remove the empty page
            lastpage.style.display = "none";

            console.log("the last page is a receiver");
          }
        }
      }
    }
  }

  afterRendered(pages) {
    document.querySelectorAll(".imageReceiver").forEach((page) => {
      if (page.querySelectorAll("figure").length == 1) {
        page.classList.add("setBottom");

        //get page height,
        //
        const pageHeight = page.querySelector(
          ".pagedjs_page_content"
        ).offsetHeight;
        console.log("height", pageHeight);

        const figureHeight = page.querySelector("figure").offsetHeight;
        console.log(figureHeight);

        if (figureHeight > pageHeight) {
          //reduce img.
          let img = page.querySelector("img");
          img.style.height = `${img.offsetHeight}px`;
          let heightToRemove = page.querySelector("figcaption").offsetHeight;

          img.style.height = `${pageHeight - heightToRemove}px`;
          // img.style.width = `auto`;
          img.style.margin = "auto";
          img.style.display = "block";
          //check if the image is out of the page
          //reduce the image size
          //
        }
      }
    });
  }
}

Paged.registerHandlers(imgPlacement);

// get the remaining space on a specific page.
function getRemainingSpaceOnPage(page) {
  const parentHeight = parseInt(window.getComputedStyle(page).height);
  return parentHeight - getLastBlockOffsetBottom(page);
}

function getLastBlockOffsetBottom(element) {
  const children = element.children;
  let lastBlockOffsetBottom = 0;

  for (let i = children.length - 1; i >= 0; i--) {
    const child = children[i];

    // Check if the child is a block-level element (by checking it’s display)
    const display = window.getComputedStyle(child).display;
    if (display === "block") {
      // Calculate the cumulative offset bottom
      const childOffsetBottom = child.offsetTop + child.offsetHeight;

      // Update the last block offset bottom if it's greater
      if (childOffsetBottom > lastBlockOffsetBottom) {
        lastBlockOffsetBottom = childOffsetBottom;
      }

      // Recursively check nested elements
      const nestedBlockOffsetBottom = getLastBlockOffsetBottom(child);
      if (nestedBlockOffsetBottom > lastBlockOffsetBottom) {
        lastBlockOffsetBottom = nestedBlockOffsetBottom;
      }
    }
  }

  // return the offset bottom in px (number)
  return lastBlockOffsetBottom;
}

function getRemainingSpace(element) {
  // get the remaining  space on the page.

  // get the page and its dimensions : get the div first child of the parent, the content-wrapper
  const parent = element.closest(".pagedjs_page_content").firstElementChild;
  const parentHeight = parseInt(window.getComputedStyle(parent).height);
  const parentWidth = parseInt(window.getComputedStyle(parent).width);

  // if the element is the first on the page, return the parentHeight
  if (!element.previousElementSibling) {
    console.log("first element on the page");
    return parentHeight;
  }

  // look for the previous element to find where it stops
  const previousElement = element.previousElementSibling;
  const previousElementBottom = parseInt(
    previousElement.offsetTop + previousElement.offsetHeight
  );

  // remaining space also need to have the marginbottom of the element
  // get the margin-bottom of the element
  const previousElementMarginBottom = parseInt(
    window.getComputedStyle(previousElement).marginBottom
  );

  // return the remaining space on the page
  const whitespace = {
    availableHeight:
      parentHeight - (previousElementBottom + previousElementMarginBottom),
    maxWidth: parentWidth,
  };
  return whitespace;
}

// get the image width and height
async function addWidthHeightToImg(parsed) {
  let imagePromises = [];
  let images = parsed.querySelectorAll("img");
  images.forEach((image) => {
    let img = new Image();
    let resolve, reject;
    let imageLoaded = new Promise(function(r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function() {
      let height = img.naturalHeight;
      let width = img.naturalWidth;
      image.setAttribute("height", height);
      image.setAttribute("width", width);

      let ratio = width / height;
      if (ratio >= 1.7) {
        image.classList.add("landscape");
        image.parentNode.classList.add("fig-landscape");
      } else if (ratio <= 0.95) {
        image.classList.add("portrait");
        image.parentNode.classList.add("fig-portrait");
      } else if (ratio < 1.39 || ratio > 0.95) {
        image.classList.add("square");
        image.parentNode.classList.add("fig-square");
      }
      resolve();
    };
    img.onerror = function() {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  return Promise.all(imagePromises).catch((err) => {
    console.warn("err", err);
  });
}

// get the homothetic reduce height when reducing the width to see if the image can get in.
// function getHeight(originalWidth, originalHeight, reducedWidth, baseline) {
//   const homotheticHeight = (reducedWidth * originalHeight) / originalWidth;
//   return homotheticHeight;
// }

// get the homothetic reduce height when reducing the width to see if the image can get in.
// with a baseline
function getHeight(originalWidth, originalHeight, reducedWidth, baseline) {
  const homotheticHeight = (reducedWidth * originalHeight) / originalWidth;
  const baselineHeight = Math.floor(homotheticHeight / baseline) * baseline;
  return Math.floor(baselineHeight);
}

function reduceImage(node, whiteSpace) {
  // node.style.float = "left";
  // node.style.marginRight = "3ch";
  // node.style.marginLeft = "-180px";
  // node.style.width = "auto";
  node.style.maxHeight = whiteSpace - 20 + "px";
}

function checkImageHeightRatio(imageHeight, availableSpace) {
  return imageHeight / availableSpace;
}

function resizeToBaseline(originalWidth, originalHeight, baselineGrid) {
  // Calculate the new height relative to the baseline grid
  const baselineHeight =
    Math.floor(originalHeight / baselineGrid) * baselineGrid;

  // Calculate the width reduction ratio based on the new height
  const reductionRatio = baselineHeight / originalHeight;

  // Calculate the new width proportionally
  const reducedWidth = originalWidth * reductionRatio;

  // Return the reduced dimensions as an object
  return {
    width: reducedWidth,
    height: baselineHeight,
  };
}

//check if the element is the the nessted first child of the page
function isNestedFirstChild(childElement, parentElement) {
  const firstChild = parentElement.firstElementChild;
  // console.log(firstChild === childElement);
  return firstChild === childElement;
}

// check if element is empty
function isElementEmpty(element) {
  return element.textContent.trim() === "";
}

//reorder figure
function reorderfigures(content) {
  content.querySelectorAll("figure .figure__label").forEach((lab) => {
    if (
      lab.closest("figure") &&
      lab.closest("figure").querySelector("h2,h3, h4 ")
    ) {
      lab
        .closest("figure")
        .querySelector("h2, h3, h4")
        .insertAdjacentElement("afterbegin", lab);
    }
  });
}

// function to move thing to a new page
//

function tagFigure(content) {
  const figures = content.querySelectorAll("figure");
  figures.forEach((figure) => {
    // console.log(figure);

    //number of images
    figure.classList.add(`imgs-${figure.querySelectorAll("img").length}`);
    //
    // is there a title
    // TODO
    // figure.classList.add(`has-title`);
    // is there a caption

    if (!figure.querySelector("figcaption p")) {
      figure.classList.add("no-caption");
    }
    // how long is the caption
    // when the caption is less than 350, makes it one column
    console.log(figure.querySelector("figcaption p")?.textContent?.length);
    if (figure.querySelector("figcaption p")?.textContent?.length < 350) {
      figure.classList.add("shortcaption");
    }
  });
}

function getLastPage() {
  // create a page variable which is the one pagedjs is working on.
  // use latest page to get the latest page
  let pages = document.querySelectorAll(".pagedjs_page");
  const latestPage = pages[pages.length - 1];
  return latestPage;
}

// WIDTH
// current image width / biggest img * 100%;

// this let us find a correspondances of width for the image we get.
// really experimental

async function calculateDesiredWidthPercentages(images) {
  // Find the width of the largest image
  let largestWidth = 0;
  images.forEach((image) => {
    const width = image.width;
    if (width > largestWidth) {
      largestWidth = width;
    }
  });

  // Calculate the desired width percentage for each image and set as data attribute
  // and set the width style
  images.forEach((image) => {
    const widthPercentage = (image.width / largestWidth) * 100;
    image.setAttribute("data-desired-width", widthPercentage);
    image.style.width = `${Math.round(widthPercentage)}%`;
  });

  // Return the modified array of image elements
  return images;
}
