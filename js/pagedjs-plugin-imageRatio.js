
  afterParsed(parsed) {
    imageRatio(parsed);
  }


function imageRatio(parsed) {
  let imagePromises = [];
  let images = parsed.querySelectorAll("img");
  images.forEach(image => {
    let img = new Image();
    let resolve, reject;
    let imageLoaded = new Promise(function(r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function() {
      let height = img.naturalHeight;

      let width = img.naturalWidth;

      let ratio = width / height;
      if (ratio >= 1.4) {
        image.classList.add("landscape");
        image.parentNode.classList.add("fig-landscape");
      } else if (ratio <= 0.8) {
        image.classList.add("portrait");
        image.parentNode.classList.add("fig-portrait");
      } else if (ratio < 1.39 || ratio > 0.8) {
        image.classList.add("square");
        image.parentNode.classList.add("fig-square");
      }

      // all images as square
      // image.classList.add("square");
      // image.parentNode.classList.add("fig-square");

      resolve();
    };
    img.onerror = function() {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  return Promise.all(imagePromises).catch(err => {
    console.warn(err);
  });
}
