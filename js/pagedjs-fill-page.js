// this script wil put any element with the property
// `position: fill page` on its own page while keeping the rest of the content flowing.
//
// use:
//
// ---===---===---
//
//   figure {
//     position: fill-page;
//     width: 100%;
//     height: 100%;
//     object-fit: cover;
//     display: block;
//   }
//   @page pagedjs-fillpage {
//     background: orange;
//     margin: 40px 100px 50px 12px;
//   }
//
// ---===---===---
//
// the pagedjs-fillpage template is created by the script to manage the layout of the fullpage layout
//
//this will try to fill up the page with any image coming up from the content
//
//

const fillPageClass = "pagedjs-fill-next-page";
class fullpage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.fillPageElements = [];
    this.pushToNextPage = [];
  }

  // before parsed put the fig number as a span in the title for each figure (if it has h2/h3/h4)
  beforeParsed(content) {
    // console.log(content);
    // move the caption around the image
    reorderfigures(content);
    tagFigure(content);
  }

  //find from the css the element you wanna have  full page
  onDeclaration(declaration, dItem, dList, rule) {
    if (declaration.property == "position") {
      if (declaration.value.children.head.data.name.includes("fill-page")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.fillPageElements.push(sel.split(","));
      }
    }
  }

  async afterParsed(content) {
    // add the width and height to the image as attribute to get more data about what they are
    await addWidthHeightToImg(content);
    await calculateDesiredWidthPercentages(
      content.querySelectorAll("figure img")
    );
    // get data-width based on biggest image, and set a data-desired-width.
    // used to get the % in the width

    //find from the css the element you wanna have  full page
    if (("this", this.fillPageElements)) {
      this.fillPageElements.forEach((selector) => {
        content.querySelectorAll(selector).forEach((el) => {
          el.classList.add(fillPageClass);
          el.style.display = "none";
          // el.style.position = "absolute";
        });
      });
    }
  }

  renderNode(node, nodeTemplate) {
    if (node.nodeType != 1) return;
    if (node.classList.contains(fillPageClass)) {
      // hide the element
      // node.style.position = "absolute";
      // do nothing when rendering nodes. we’ll do something else
    }
  }

  // for column safety
  async finalizePage(pageFragment, oldpage) {
    // if the page has no element with the fillpageclass
    if (!oldpage.element.querySelector(`.${fillPageClass}`))
      console.log("page without anything to push to the nextpage");
    // else push that element to next full page
    pushItToNextFullPage(pageFragment, oldpage, this.chunker);
  }

  afterRendered(pages) {
    // add the running head
    let runninghead = document.querySelector(".runninghead");

    document
      .querySelectorAll(".pagedjs_pagedjs-filler_page")
      .forEach((page) => {
        // this is for elife only
        if (runninghead) {
          page
            .querySelector(".pagedjs_pagebox")
            .insertAdjacentElement("afterbegin", runninghead.cloneNode(true));
        }
        // check if the page has only one figure
        if (page.querySelectorAll("figure").length == 1) {
          // fix the height of elements
          //get page height,
          const pageHeight = page.querySelector(
            ".pagedjs_page_content"
          ).offsetHeight;

          const figureHeight = page.querySelector("figure").offsetHeight;

          if (figureHeight > pageHeight) {
            //reduce img.
            let img = page.querySelector("img");
            img.style.height = `${img.offsetHeight}px`;
            let heightToRemove = page.querySelector("figcaption").offsetHeight;

            img.style.height = `${pageHeight - heightToRemove}px`;
            // img.style.width = `auto`;
            img.style.margin = "auto";
            img.style.display = "block";
            //check if the image is out of the page
            //reduce the image size
            //
          }

          page.querySelector(".pagedjs_page_content").style.alignItems =
            "center";
          page.querySelector(".pagedjs_page_content").style.justifyContent =
            "center";
          page.querySelector(".pagedjs_page_content").style.display = "flex";
        }
      });
  }
}

Paged.registerHandlers(fullpage);

async function pushItToNextFullPage(pageFragment, oldpage, chunker) {
  //make sure you’re not checkintg the added page
  // script to run in the async finalize page
  if (oldpage.element.classList.contains("addedpage")) {
    return console.log("dont run the page twice");
  }

  // try to put to the next page, if there is not enough room, create a new page until the element is empty

  // the spread operator to get the list of all the element to push to the next page
  let elementsList = [...pageFragment.querySelectorAll(`.${fillPageClass}`)];

  while (elementsList.length > 0) {
    // check if the last page has a class of
    // pagedjs_pagedjs-filler_page

    // console.log("element list item", elementsList[0], elementsList.length);

    // check if the last page
    const lastpage = getLastPage();

    //if the last page contains the classsName of an added page
    if (lastpage.classList.contains("pagedjs_pagedjs-filler_page")) {
      // console.log("the last page is ready to get some content");
      // a page exist with a filler class
      //show the element hidden before
      elementsList[0].style.display = "block";
      // elementsList[0].style.position = "unset";

      // let see how much space is left
      const remainingSpace = await getRemainingSpaceOnPage(
        lastpage.querySelector(".pagedjs_page_content")
      );
      const elementHeight = await getHeightOfHiddenElement(elementsList[0]);

      // if there is enough space, put the block
      if (remainingSpace > elementHeight) {
        lastpage
          .querySelector(".pagedjs_page_content")
          .insertAdjacentElement("beforeend", elementsList[0]);
      } else {
        // otherwise create a new page
        const newpage = await chunker.addPage();

        // emulate the beforepage lyout to add the page to the flow pagedjs is waiting for
        await chunker.hooks.beforePageLayout.trigger(
          newpage,
          undefined,
          undefined,
          chunker
        );

        // tell pagedjs that a new page has been set
        chunker.emit("page", newpage);

        newpage.element.classList.add("pagedjs_pagedjs-filler_page");
        newpage.element
          .querySelector(".pagedjs_page_content")
          .insertAdjacentElement("afterbegin", elementsList[0]);
        // create a new page and fill it up
        // console.log(lastpage);
      }

      // create a new page and fill it up
    } else if (!lastpage.classList.contains("pagedjs_pagedjs-filler_page")) {
      //prepare the elements
      elementsList[0].style.display = "block";
      elementsList[0].style.position = "unset";

      const newpage = await chunker.addPage();
      newpage.element.classList.add("pagedjs_pagedjs-filler_page");
      newpage.element
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement("afterbegin", elementsList[0]);
      // create a new page and fill it up
      // console.log(lastpage);
    } else {
      console.log("there is no new page damsn");
    }

    // do the next figure
    elementsList.shift();
  }
}

async function addWidthHeightToImg(parsed) {
  let imagePromises = [];
  let images = parsed.querySelectorAll("img");
  images.forEach((image) => {
    let img = new Image();
    let resolve, reject;
    let imageLoaded = new Promise(function (r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function () {
      let height = img.naturalHeight;
      let width = img.naturalWidth;
      image.setAttribute("height", height);
      image.setAttribute("width", width);

      let ratio = width / height;
      if (ratio >= 1.7) {
        image.classList.add("landscape");
        image.parentNode.classList.add("fig-landscape");
      } else if (ratio <= 0.95) {
        image.classList.add("portrait");
        image.parentNode.classList.add("fig-portrait");
      } else if (ratio < 1.39 || ratio > 0.95) {
        image.classList.add("square");
        image.parentNode.classList.add("fig-square");
      }
      resolve();
    };
    img.onerror = function () {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  return Promise.all(imagePromises).catch((err) => {
    console.warn("err", err);
  });
}

// get the homothetic reduce height when reducing the width to see if the image can get in.
// with a baseline
function getHeight(originalWidth, originalHeight, reducedWidth, baseline) {
  const homotheticHeight = (reducedWidth * originalHeight) / originalWidth;
  const baselineHeight = Math.floor(homotheticHeight / baseline) * baseline;
  return Math.floor(baselineHeight);
}

function checkImageHeightRatio(imageHeight, availableSpace) {
  return imageHeight / availableSpace;
}

function resizeToBaseline(originalWidth, originalHeight, baselineGrid) {
  // Calculate the new height relative to the baseline grid
  const baselineHeight =
    Math.floor(originalHeight / baselineGrid) * baselineGrid;

  // Calculate the width reduction ratio based on the new height
  const reductionRatio = baselineHeight / originalHeight;

  // Calculate the new width proportionally
  const reducedWidth = originalWidth * reductionRatio;

  // Return the reduced dimensions as an object
  return {
    width: reducedWidth,
    height: baselineHeight,
  };
}

//check if the element is the the nessted first child of the page
function isNestedFirstChild(childElement, parentElement) {
  const firstChild = parentElement.firstElementChild;
  // console.log(firstChild === childElement);
  return firstChild === childElement;
}

// check if element is empty
function isElementEmpty(element) {
  return element.textContent.trim() === "";
}

//reorder figure
function reorderfigures(content) {
  content.querySelectorAll("figure .figure__label").forEach((lab) => {
    if (
      lab.closest("figure") &&
      lab.closest("figure").querySelector("h2,h3, h4 ")
    ) {
      lab
        .closest("figure")
        .querySelector("h2, h3, h4")
        .insertAdjacentElement("afterbegin", lab);
    }
  });
}

// function to move thing to a new page
//
//
//

function getLastBlockOffsetBottom(element) {
  const children = element.children;
  let lastBlockOffsetBottom = 0;

  for (let i = children.length - 1; i >= 0; i--) {
    const child = children[i];

    // Check if the child is a block-level element (by checking it’s display)
    const display = window.getComputedStyle(child).display;
    if (display === "block") {
      // Calculate the cumulative offset bottom
      const childOffsetBottom = child.offsetTop + child.offsetHeight;

      // Update the last block offset bottom if it's greater
      if (childOffsetBottom > lastBlockOffsetBottom) {
        lastBlockOffsetBottom = childOffsetBottom;
      }

      // Recursively check nested elements
      const nestedBlockOffsetBottom = getLastBlockOffsetBottom(child);
      if (nestedBlockOffsetBottom > lastBlockOffsetBottom) {
        lastBlockOffsetBottom = nestedBlockOffsetBottom;
      }
    }
  }

  // return the offset bottom in px (number)
  return lastBlockOffsetBottom;
}
async function getRemainingSpaceOnPage(page) {
  const parentHeight = parseInt(window.getComputedStyle(page).height);
  return parentHeight - getLastBlockOffsetBottom(page);
}

function tagFigure(content) {
  const figures = content.querySelectorAll("figure");
  figures.forEach((figure) => {
    // console.log(figure);

    //number of images
    figure.classList.add(`imgs-${figure.querySelectorAll("img").length}`);
    //
    // is there a title
    // TODO
    // figure.classList.add(`has-title`);
    // is there a caption

    if (!figure.querySelector("figcaption p")) {
      figure.classList.add("no-caption");
    }
    // how long is the caption
    // when the caption is less than 550, makes it one column
    if (figure.querySelector("figcaption p")?.textContent?.length < 550) {
      figure.classList.add("shortcaption");
    } else {
      figure.classList.add("longcaption");
    }
  });
}

function getLastPage() {
  // create a page variable which is the one pagedjs is working on.
  // use latest page to get the latest page
  let pages = document.querySelectorAll(".pagedjs_page");
  const latestPage = pages[pages.length - 1];
  return latestPage;
}

// WIDTH
// current image width / biggest img * 100%;

// this let us find a correspondances of width for the image we get.
// really experimental

async function calculateDesiredWidthPercentages(images) {
  // Find the width of the largest image
  let largestWidth = 0;
  images.forEach((image) => {
    const width = image.width;
    if (width > largestWidth) {
      largestWidth = width;
    }
  });

  // Calculate the desired width percentage for each image and set as data attribute
  // and set the width style
  images.forEach((image) => {
    const widthPercentage = (image.width / largestWidth) * 100;
    image.setAttribute("data-desired-width", widthPercentage);
    image.style.width = `${Math.round(widthPercentage)}%`;
  });

  // Return the modified array of image elements
  return images;
}

async function getHeightOfHiddenElement(element) {
  // find a page to render on
  let pageToRenderOn = getLastPage();

  let clone = element.cloneNode(true);
  clone.style.position = "absolute";
  clone.style.top = "0";
  clone.style.left = "0";
  clone.style.display = "block";

  // insert a clone of that element
  pageToRenderOn
    .querySelector(".pagedjs_page_content")
    .insertAdjacentElement("afterbegin", clone);

  // get margin in case there are somex
  //
  let margins = clone.style.marginTop + clone.style.marginBottom;

  let elementHeight = clone.offsetHeight + margins;
  clone.remove();
  return elementHeight;
}

async function putOnPage(element, page) {
  // check if there is enough room to put an element on that page
  let availableSpace = getRemainingSpaceOnPage(page);

  // you can’t know the right width and hwgiht of the element because it’s never been rendered.
  // option1 : set a position: absolute; top: 0, leftz. 0, check its size
  // render a clone of the element to get the height of the element
  let elHeight = await getHeightOfHiddenElement(element);

  // if there is enough room on the page
  if (availableSpace > elHeight) {
    const clone = element.cloneNode(true);
    lastpage
      .querySelector(".pagedjs_page_content")
      .insertAdjacentElement("afterbegin", clone);
    return true;
  }
}
