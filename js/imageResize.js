/* resize image base on bigger image and baseline */

function resizeImage(originalWidth, originalHeight, reductionRatio, baseline) {
  const newWidth = originalWidth * reductionRatio;
  const aspectRatio = originalWidth / originalHeight;
  const newHeight = Math.ceil(newWidth / aspectRatio);
  const heightMultiple = Math.ceil(newHeight / baseline) * baseline;
  return {
    width: newWidth,
    height: heightMultiple
  };
}
// Function to get the reduction ratio based on the largest image
function getReductionRatio(largestWidth, largestHeight, originalWidth, originalHeight) {
  const reductionRatioWidth = largestWidth / originalWidth;
  const reductionRatioHeight = largestHeight / originalHeight;
  return Math.min(reductionRatioWidth, reductionRatioHeight);
}

// Example usage
const largestWidth = 800;
const largestHeight = 600;
const images = [
  { width: 1200, height: 900 },
  { width: 600, height: 400 },
  { width: 1600, height: 1200 }
];
const baseline = 17;

const reductionRatio = getReductionRatio(largestWidth, largestHeight, images[0].width, images[0].height);
const resizedImages = images.map(image => resizeImage(image.width, image.height, reductionRatio, baseline));

resizedImages.forEach(resizedImage => {
  console.log(`Resized Width: ${resizedImage.width}`);
  console.log(`Resized Height: ${resizedImage.height}`);
});

