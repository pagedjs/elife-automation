// Set the handler
class elifeBuild extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    getTable(content);
    removeNoScript(content);
    hidenumber(content);
    addIDtoEachElement(content);
    removescript(content);
    spaceToNbsp(content);
    trimArticleFlags(content);
    link2spanWithWbr(content);
    createElForRunningHead(content); // ←
    // splitCitationContentAndLink(content);
    addDateBeforeDOI(content);
    // // moveSummary(content)
    // // removeMeta(content)
    // addClassToConsecutiveLinks(content)
    // movelabelToCaption(content);
    moveTheSecondColumnToTop(content); //
    // addCorrespondingAuthor(content)
    // // reorderReview(content)
    addLogoWithLink(content);
    wrapReferences(content);
  }

  afterParsed(parsed) {
    imageRatio(parsed);
    formulaeTest(parsed);
  }
}

Paged.registerHandlers(elifeBuild);

function wrapReferences(content) {
  content
    .querySelector("#references")
    .closest("section")
    .classList.add("reference-section");
}


function getTable(content) {
  let images = content.querySelectorAll("img");
  images.forEach((image) => {
    const figure = image.closest("figure");
    // console.log("img", image);
    // console.log("fig", figure);
    if (
      figure?.id.includes("tbl") ||
      figure?.id.includes("tab") ||
      figure?.id.includes("utbl")
    ) {
      figure.classList.add("imageTable");
      image.innerHTML = `<figure class="sub">${image.innerHTML}</figure>`;
      if (figure?.children[0].nodeName !== "FIGCAPTION") {
        const tblCaption = figure.querySelector("figcaption");
        tblCaption && figure.insertAdjacentElement("afterbegin", tblCaption);
      }
    }
  });
}

function trimArticleFlags(content) {
  content.querySelectorAll(".article-flag-list li")?.forEach((item) => {
    item.textContent = item.textContent.trim();
  });
}

function removeNoScript(content) {
  content.querySelectorAll("noscript").forEach((thing) => {
    thing.remove();
  });
}

function link2spanWithWbr(content) {
  content.querySelectorAll("a").forEach((link) => {
    if (link.href.includes("#")) {
      link.classList.add("internalLink");
    }
    if (link.href.includes("#c")) {
      link.classList.add("referecenceLink");
      if (link.previousSibling?.href?.includes("#c")) {
        link.classList.add("nextElement");
      }
    } else if (link.textContent.includes("http")) {
      link.classList.add("content-link");
      link.innerHTML = formatUrl(link.innerHTML);
    } else {
      if (
        !link.classList.contains("article-status__link") &&
        !link.classList.contains("author-list__orcids_link")
      ) {
        const doi = document.querySelector("body").dataset.doi,
          testStr = `http://localhost:8080/${doi
            .split("/")
            .join("")
            .toLowerCase()}`;
        const doiArr = doi.split("."),
          elifeURL = `https://elifesciences.org/reviewed-preprints/${doiArr[2]}v${doiArr[3]}`;

        const href = link.href.includes(testStr)
          ? link.href.replace(testStr, elifeURL)
          : link.href;
        // console.log(href);
        link.insertAdjacentHTML(
          "beforeend",
          `<span class="prettified-symbol">(</span><span class="prettified-link">${formatUrl(
            href
          )}</span><span class="prettified-symbol">)</span>`
        );

        link.href = link.href.includes(testStr)
          ? link.href.replace(`${testStr}/`, "")
          : link.href;
      }
    }
  });
}

function hidenumber(content) {
  content.querySelectorAll("p").forEach((p) => {
    if (!p.innerHTML.match(/[A-z]/g)) {
      p.classList.add("onlynumbers");
    }
  });
}

function addCorrespondingAuthor(content) {
  if (content.querySelector(".contributors .corresp-list")) {
    content
      .querySelector(".institutions_institutions-list__g1Bu7")
      .insertAdjacentHTML(
        "afterend",
        content.querySelector(".contributors .corresp-list").outerHTML
      );
    content.querySelector(".corresp-list").id = "corresponding";
    content.querySelector(".corresp-list").dataset.id = "corresponding";
    content.querySelector(".corresp-list").classList.add("corresponding");
  }
  if (content.querySelector(".contributors .contributor-list")) {
    const existingAuthorsList = content.querySelector(
      ".authors_authors-list__qIQOP"
    );
    existingAuthorsList.insertAdjacentHTML(
      "afterend",
      content.querySelector(".contributors .contributor-list").outerHTML
    );
    // existingAuthorsList.classList.add('hide')
    existingAuthorsList.remove();
    content
      .querySelector(".contributor-list")
      .classList.add("authors_authors-list__qIQOP");
    content
      .querySelectorAll(".contributor-list li")
      .forEach((li) => li.classList.add("authors_authors-list__item__tb24B"));
  }
  if (content.querySelector(".contributors .affiliation-list")) {
    const existingAffliationsList = content.querySelector(
      ".institutions_institutions-list__g1Bu7"
    );
    existingAffliationsList.insertAdjacentHTML(
      "afterend",
      content.querySelector(".contributors .affiliation-list").outerHTML
    );
    // existingAffliationsList.classList.add('hide')
    existingAffliationsList.remove();
    content
      .querySelector(".affiliation-list")
      .classList.add("institutions_institutions-list__g1Bu7");
    content
      .querySelectorAll(".affiliation-list li")
      .forEach((li) =>
        li.classList.add("institutions_institutions-list__item__0P_F0")
      );
  }
  // content.querySelector('.contributors').classList.add('hide')
  content.querySelector(".contributors").remove();
}

function addClassToConsecutiveLinks(content) {
  content.querySelectorAll("span a + a").forEach((link) => {
    link.classList.add("nextlink");
  });
}

function reorderReview(content) {
  let reviewsOrdered = document.createElement("section");
  reviewsOrdered.classList.add("review-ordered");
  let wrap = Array.from(
    content
      .querySelector("#editors-and-reviewers")
      .closest("div")
      .querySelectorAll("section")
  );
  // console.log(wrap);
  for (let index = 0; index < wrap.length; index++) {
    const element = wrap[index];
    // console.log(element.className);
    // console.log(element);
    if (element.className.includes("review-content_review-content")) {
      reviewsOrdered.insertAdjacentElement("afterbegin", element);
    }
  }
  // console.log(reviewsOrdered);
  content
    .querySelector("#editors-and-reviewers")
    .closest("div")
    .insertAdjacentElement("beforeend", reviewsOrdered);
  // wrap.querySelectorAll('section').forEach( review =>{
  //
  //   console.log(review)
  // })
}

function addTitleToPeerReview(content) {
  content
    .querySelector("#editors-and-reviewers")
    .closest("section")
    .classList.add("peerreviews");
  content
    .querySelector("#editors-and-reviewers")
    .closest("section")
    .insertAdjacentHTML("afterbegin", `<h1>Peer review</h1>`);
}

function boldTermsInAssessment(content) {
  let block = content.querySelector("#evaluation-summary").closest("section");
  const terms = [
    "tour-de-force",
    "compelling",
    "convincing",
    "solid",
    "incomplete",
    "inadequate",
    "landmark",
    "fundamental",
    "important",
    "noteworthy",
    "useful",
    "flawed",
  ];
  terms.forEach((term) => {
    block.innerHTML = block.innerHTML.replace(term, `<strong>${term}</strong>`);
  });
}

function changeReviewSummaryTitle(content) {
  content.querySelector("#evaluation-summary").innerHTML = "eLife assessment";
}

function moveBlocPageOne(pages) {
  const offset = -40;

  const block = document.querySelector(".colBlock");
  let bottom = block
    .closest(".pagedjs_page_content")
    .querySelector("div").lastChild;
  var bounding = bottom.getBoundingClientRect();
  var blockBounding = block.getBoundingClientRect();
  // console.log(bounding);
  // console.log(blockBounding);
  block.style.top = `${bounding.bottom - bounding.top - blockBounding.height
    }px`;
  // console.log(bounding);
}

function moveTheSecondColumnToTop(content) {
  const host = content.querySelector("#__next");
  host.insertAdjacentHTML(
    "beforebegin",
    `<section class="colBlock">${content.querySelector("aside").innerHTML
    }</section>`
  );
  content.querySelector("aside").style.display = "none";
}

function movelabelToCaption(content) {
  content.querySelectorAll("figure").forEach((fig) => {
    const label = fig.querySelector("label");
    const caption = fig.querySelector("figcaption");
    // fig.insertAdjacentElement("afterbegin", caption)
    caption.innerHTML = `${caption.innerHTML}`;
    if (label && caption) {
      caption.classList.add("figcaption");
      caption.insertAdjacentElement("afterbegin", label);
    }
    // console.log(caption)
  });
}

function removeMeta(content) {
  content.querySelectorAll("meta").forEach((el) => el.remove());
}

function addDateBeforeDOI(content) {
  // the date
  //
  let date = "";
  content.querySelectorAll("dd").forEach((dd) => {
    if (dd.className.includes("timeline__date") && date === "") {
      dd.querySelector("a")?.remove();
      return (date = dd.textContent);
    }
  });

  // find the doi link

  content.querySelectorAll("a").forEach((link) => {
    if (
      link.href.includes("doi.org") &&
      !link.classList.contains("reference__doi_link")
    ) {
      let year = new Date(date);
      link.insertAdjacentHTML(
        "afterbegin",
        `<span class="date">${year.getFullYear()} eLife</span>. `
      );
    }
  });
  // content
  //   .querySelector('.content-header__identifier')?
  //   .insertAdjacentHTML(
  //     'afterbegin',
  //     `<span class="date">${
  //       content.querySelector('.review-timeline__date').innerHTML
  //     }</span>, `
  //   )
}

// get the image ratio to define a first layout
//
async function imageRatio(parsed) {
  let imagePromises = [];
  let images = parsed.querySelectorAll("img");
  images.forEach((image) => {
    const figure = image.closest("figure");
    // added that tp the before parsed
    // if (figure?.id.includes("tbl")) {
    //   figure.classList.add("imageTable");
    //   if (figure?.children[0].nodeName !== "FIGCAPTION") {
    //     const tblCaption = figure.querySelector("figcaption");
    //     tblCaption && figure.insertAdjacentElement("afterbegin", tblCaption);
    //   }
    // }
    if (figure?.id.includes("alg")) {
      // for algorithms
      figure.classList.add("imageAlgorithm");
      if (figure?.children[0].nodeName !== "FIGCAPTION") {
        const algoCaption = figure.querySelector("figcaption");
        algoCaption && figure.insertAdjacentElement("afterbegin", algoCaption);
      }
    }
    let img = new Image();
    let resolve, reject;
    let imageLoaded = new Promise(function(r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function() {
      if (
        image.closest("figure") &&
        (image.closest("figure").classList.contains("imageTable") ||
          image.closest("figure").classList.contains("imageAlgorithm"))
      ) {
        // return console.log("the image has no figure", image);
      }

      let height = img.naturalHeight;

      let width = img.naturalWidth;

      let ratio = width / height;
      if (ratio >= 1.7) {
        image.classList.add("landscape");
        image.parentNode.classList.add("fig-landscape");
      } else if (ratio <= 0.95) {
        image.classList.add("portrait");
        image.parentNode.classList.add("fig-portrait");
      } else if (ratio < 1.39 || ratio > 0.95) {
        image.classList.add("square");
        image.parentNode.classList.add("fig-square");
      }
      resolve();
    };
    img.onerror = function() {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  return Promise.all(imagePromises).catch((err) => {
    console.warn("err", err);
  });
}

//  TODO: try to fix the multiple reak-after: avoid;
function fixBreak(page) {
  let final = page.lastChild;
  if (page.querySelector('[data-break-after="avoid"]')) {
  }
}

// To use to remove hyphens between pages
function getFinalWord(words) {
  var n = words.split(" ");
  return n[n.length - 1];
}

// add nbsp in list
function spaceToNbsp(content) {
  let authorList = content.querySelectorAll(
    ".authors li, .authors_authors-list__qIQOP li"
  );
  // let affiliationList = content.querySelector('.authors').nextElementSibling.querySelectorAll('li')

  const replaceSpace = (parent) => {
    if (parent.children.length)
      Array.from(parent.children).forEach((child) => replaceSpace(child));
    else parent.innerHTML = parent.innerHTML.replace(/\s+/g, "&nbsp;").trim();
  };
  authorList.forEach((person) => replaceSpace(person));
}

// add elife caution
function addElifeTextBeforeAbstract(content) {
  let abstract = content
    .querySelector("#evaluation-summary")
    .closest("section");
  abstract.insertAdjacentHTML(
    "beforebegin",
    `<section class="elife-caution">${content.querySelector(".evaluation-summary").innerHTML
    }</section>`
  );
  content.querySelector(".evaluation-summary").style.display = "none";
  // querySelector("#abstract")?.closest('section');
}
//move summary
function moveSummary(content) {
  let summaries = content.querySelectorAll(".evaluation-summary");
  content
    .querySelector("#abstract")
    .closest("section")
    .insertAdjacentElement("afterend", summaries[0]);
}
// rmeove scripts

function removescript(content) {
  let data = content.querySelector("#__next");
  data.innerHTML = data.innerHTML.replace(/\<\!-+\s?\-+\>/g, "");
  content.querySelectorAll("script").forEach((script) => {
    script.innerHTML = "";
    script.remove();
  });
}

// create a table of content

let toctags = "h2, h3";

function createTOC(content) {
  let nav = document.createElement("nav");
  nav.innerHTML = "<h2>Table of contents</h2>";
  let toclist = document.createElement("ul");
  content.querySelectorAll(toctags).forEach((tag) => {
    toclist.insertAdjacentHTML(
      "beforeend",
      `<li class="toc-${tag.tagName.toLowerCase()}"><a href="#${tag.id
      }">${tag.innerHTML.trim()}</a></li>`
    );
  });
  nav.insertAdjacentElement("beforeend", toclist);
  content
    .querySelector(".evaluation-summary")
    .insertAdjacentElement("afterend", nav);
}

function splitCitationContentAndLink(content) {
  const links = content.querySelectorAll("a");
  links?.forEach((link) => {
    if (link.href.includes("#c")) {
      const hasParentSup = link.closest("sup");
      const linkText = link.textContent;

      if (hasParentSup || !isNaN(linkText)) return;
      const sup = document.createElement("sup");
      link.classList.add("updatedLink");
      //  const linkedElem = content.querySelector(`#${link.href.split('#')[1]}`)
      //  console.log(linkedElem);
      link.textContent = `${link.href.split("#c")[1]}`;

      const span = document.createElement("span");
      span.classList.add("linkText");
      span.textContent = linkText;
      if (link.classList.contains("nextElement"))
        span.classList.add("nextElement");
      sup.append(link.cloneNode(true));
      link.insertAdjacentElement("beforeBegin", span);
      link.replaceWith(sup);
    }
  });
}

/*========================== 
     addIDtoEachElement
========================== */

// Define here the tags you want to give id
let tags = [
  "figure",
  "figcaption",
  "img",
  "ol",
  "ul",
  "li",
  "p",
  "img",
  "table",
  "h1",
  "h2",
  "h3",
  "h4",
  "div",
  "aside",
];

function addIDtoEachElement(content) {
  let total = 0;
  tags.forEach((tag) => {
    content.querySelectorAll(tag).forEach((el, index) => {
      if (!el.id) {
        if (el.tagName == "p") {
          if (el.closest("figcaption")) {
            return;
          }
        }

        el.id = `el-${el.tagName.toLowerCase()}-${index}`;
        total++;
      }
    });
  });
}

/*========================== 
     addLogoWithLink
========================== */

function addLogoWithLink(content) {
  const elem = `
    <a href="https://elifesciences.org/" class="linkElife">
      <img src="https://elifesciences.org/assets/patterns/img/patterns/organisms/elife-logo-xs.fd623d00.svg" alt='Elife' />
    </a> `;

  content.querySelector("div").insertAdjacentHTML("afterbegin", elem);
}
/*========================== 
     openAllDetailsAndSummary
========================== */

function openAllDetailsAndSummary(content) {
  content.querySelectorAll("details").forEach((detail) => {
    detail.setAttribute("open", "open");
  });
}

/*====================================== 
     fix citation
====================================== */

function citeFix(content) {
  let bibliography = document.createElement("ul");

  bibliography.classList.add("bibliography");
  bibliography.innerHTML = "<h2>References</h2>";
  content.querySelectorAll("cite").forEach((cite) => {
    bibliography.insertAdjacentHTML(
      "beforeend",
      `<li>${cite.innerHTML.trim()}</li>`
    );
  });
  content.append(bibliography);
}

/*====================================== 
     duplicate and move figures
====================================== */

function moveFigures(content) {
  let figures = document.createElement("section");

  figures.classList.add("figureWrapper");
  figures.innerHTML = "<h2>Figures</h2>";
  content.querySelectorAll("figure").forEach((figure) => {
    figures.insertAdjacentHTML(
      "beforeend",
      `<figure class="movedfigure">${figure.innerHTML.trim()}</figure>`
    );
  });
  content.append(figures);
}

/*====================================== 
     Create the element used for runningHead 
====================================== */

function createElForRunningHead(content) {
  //doi is coming from the document body
  let author = "";
  let authors = content.querySelectorAll(
    ".authors li, .authors_authors-list__qIQOP li"
  );
  if (authors.length >= 4) {
    if (
      authors[0].children.length &&
      authors[0]?.children[0].nodeName !== "LINK"
    ) {
      const link = authors[0].querySelector("a");
      const authorName = link?.innerHTML?.trim().split("\n");
      author = `<a href=${link.href} class=${link.className}>${authorName[authorName.length - 1]
        }</a> <em> et al. </em>`;
    } else {
      const authorName = authors[0].innerHTML.split("&nbsp;");
      author = `${authorName[authorName.length - 1]} <em> et al. </em>`;
    }
  } else {
    for (let i = 0; i < authors.length; i++) {
      author = `${author}${i < authors.length - 1
          ? authors[i]?.innerHTML
          : ", " + authors[i]?.innerHTML
        }`;
    }
  }

  // let elifelink = window.location
  // const doi = elifelink.pathname.substring(0, elifelink.pathname.length - 1);
  const doi = document.querySelector("body").dataset.doi;
  content.querySelector("div").insertAdjacentHTML(
    "afterbegin",
    `<p class='runninghead'><span class="author">${author}</span>,&nbsp;<span class="doi">
      ${doi ? `<a href="https://doi.org/${doi}">https://doi.org/${doi}</a>` : ""
    }</span> <span class="counter"></p></p>`
  );
}

function removeFigureList(content) {
  content.querySelector("#figures").closest("div").remove();
}

// get ratio for images and add classes based on taht.

// no hyphens between page
class noHyphenBetweenPage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.hyphenToken;
  }

  afterPageLayout(pageFragment, page, breakToken) {
    if (pageFragment.querySelector(".pagedjs_hyphen")) {
      // find the hyphenated word
      let block = pageFragment.querySelector(".pagedjs_hyphen");

      // i dont know what that line was for :thinking: i removed it
      // block.dataset.ref = this.prevHyphen;

      // move the breakToken
      let offsetMove = getFinalWord(block.innerHTML).length;

      // move the token accordingly
      page.breakToken = page.endToken.offset - offsetMove;

      // remove the last word
      block.innerHTML = block.innerHTML.replace(
        getFinalWord(block.innerHTML),
        ""
      );

      breakToken.offset = page.endToken.offset - offsetMove;
    }
  }
}

Paged.registerHandlers(noHyphenBetweenPage);

class pushThings extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.pushblock = [];
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // move the element to the next bit
    if (declaration.property == "--experimental-push") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.pushblock.push([elId, declaration.value.value]);
      });
    }
  }
  afterParsed(parsed) {
    if (this.pushblock.length > 0) {
      this.pushblock.forEach((elToPush) => {
        const elem = parsed.querySelector(elToPush[0]);
        if (!elem) {
          return;
        }

        elem.dataset.pushBlock = elToPush[1];
        let direction = "";
        if (elToPush[1] < 0) {
          direction = "back";
        }
        if (direction == "back") {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.previousElementSibling) {
              elem.previousElementSibling.insertAdjacentElement(
                "beforebegin",
                elem
              );
            }
          }
        } else {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.nextElementSibling) {
              elem.nextElementSibling.insertAdjacentElement(
                "beforebegin",
                elem
              );
            }
          }
        }
      });
    }
  }
}

Paged.registerHandlers(pushThings);

class urlSwitcher extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    const imageUrl = document.body.id;
    content.querySelectorAll("img").forEach((img) => {
      img.src =
        "/images/" +
        imageUrl +
        "/" +
        img.src.split("/")[img.src.split("/").length - 1];
    });
    // find a place to put the content, i would say after the index
  }
}

// Paged.registerHandlers(urlSwitcher);

class CSStoClass extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatSameTop = [];
    this.floatSameBottom = [];
    this.floatNextTop = [];
    this.floatNextBottom = [];
    this.experimentalImageEdit = [];
    this.spacing = [];
    this.pushblock = [];
    this.experimentalMerged = [];
    this.fullPageBackground = [];
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // alter the image
    if (declaration.property == "--experimental-image-edit") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.experimentalImageEdit.push(elId);
      });
    }
    // move the element to the next bit
    else if (declaration.property == "--experimental-push") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.pushblock.push([elId, declaration.value.value]);
      });
    } else if (declaration.property == "--experimental-fullpage") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.fullPageBackground.push([elId, declaration.value.value]);
      });
    }
    //experimental merge
    else if (declaration.property == "--experimental-merge") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.experimentalMerged.push([elId, declaration.value.value]);
      });
    }
    // page floats
    else if (declaration.property == "--experimental-page-float") {
      if (declaration.value.value.includes("same-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameTop.push(sel.split(","));
        console.log("floatSameTop: ", this.floatSameTop);
      } else if (declaration.value.value.includes("same-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameBottom.push(sel.split(","));
        //console.log("floatSameBottom: ", this.floatSameBottom);
      } else if (declaration.value.value.includes("next-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextTop.push(sel.split(","));
        //console.log('floatNextTop: ', this.floatNextTop);
      } else if (declaration.value.value.includes("next-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextBottom.push(sel.split(","));
        //console.log("floatNextBottom: ", this.floatNextBottom);
      }
    }
    // spacing
    else if (declaration.property == "--experimental-spacing") {
      var spacingValue = declaration.value.value;
      spacingValue = spacingValue.replace(/\s/g, "");
      spacingValue = parseInt(spacingValue);
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      var thisSpacing = [sel.split(","), spacingValue];
      this.spacing.push(thisSpacing);
    }
  }

  afterParsed(parsed) {
    if (this.pushblock.length > 0) {
      this.pushblock.forEach((elToPush) => {
        const elem = parsed.querySelector(elToPush[0]);
        if (!elem) {
          return;
        }

        elem.dataset.pushBlock = elToPush[1];
        let direction = "";
        if (elToPush[1] < 0) {
          direction = "back";
        }
        if (direction == "back") {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.previousElementSibling) {
              elem.previousElementSibling.insertAdjacentElement(
                "beforebegin",
                elem
              );
            }
          }
        } else {
          for (let index = 0; index < Math.abs(elToPush[1]); index++) {
            if (elem.nextElementSibling) {
              elem.nextElementSibling.insertAdjacentElement("afterend", elem);
            }
          }
        }
      });
    }
    if (this.experimentalMerged.length > 0) {
      this.experimentalMerged.forEach((couple) => {
        const host = parsed.querySelector(couple[0]);
        const guest = parsed.querySelector(couple[1]);
        if (!host || !guest) {
          return;
        }
        guest.style.display = "none";
        host.classList.add("merged!");
        host.dataset.mergedGuest = guest.id;
        host.insertAdjacentHTML("beforeend", guest.innerHTML);
      });
    }
    if (this.experimentalImageEdit.length > 0) {
      this.experimentalImageEdit.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((img) => {
          img.classList.add("imageMover");

          // console.log("#" + img.id + ": image Mover");
        });
      });
    }
    if (this.floatNextBottom) {
      this.floatNextBottom.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-bottom");
          // console.log("#" + el.id + " moved to next-bottom");
        });
      });
    }
    if (this.floatNextTop) {
      this.floatNextTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-top");
          // console.log("#" + el.id + " moved to next-top");
        });
      });
    }
    if (this.floatSameTop) {
      this.floatSameTop.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-same-top");
          // console.log("#" + el.id + " moved to same-top");
        });
      });
    }
    if (this.floatSameBottom) {
      this.floatSameBottom.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-same-bottom");
          // console.log("#" + el.id + " moved to same-bottom");
        });
      });
    }
    if (this.spacing) {
      this.spacing.forEach((elNBlist) => {
        parsed.querySelectorAll(elNBlist[0]).forEach((el) => {
          var spacingValue = elNBlist[1];
          var spacingClass = "spacing-" + spacingValue;
          // console.log(spacingClass);
          el.classList.add(spacingClass);
          // console.log("#" + el.id + " spaced " + spacingValue);
        });
      });
    }
    if (this.fullPageBackground) {
      this.fullPageBackground.forEach((background) => {
        parsed.querySelectorAll(background[0]).forEach((el) => {
          el.classList.add("moveToBackgroundImage");
        });
      });
    }
  }
}

Paged.registerHandlers(CSStoClass);

//float top

// lets you manualy add classes to some pages elements
// to simulate page floats.
// works only for elements that are not across two pages

let classElemFloatSameTop = "page-float-same-top"; // â† class of floated elements on same page
let classElemFloatSameBottom = "page-float-same-bottom"; // â† class of floated elements bottom on same page

let classElemFloatNextTop = "page-float-next-top"; // â† class of floated elements on next page
let classElemFloatNextBottom = "page-float-next-bottom"; // â† class of floated elements bottom on next page

class elemFloatTop extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.experimentalFloatNextTop = [];
    this.baseline = 22;
    this.experimentalFloatNextBottom = [];
    this.token;
  }

  layoutNode(node) {
    // If you find a float page element, move it in the array,
    if (node.nodeType == 1 && node.classList.contains(classElemFloatNextTop)) {
      let clone = node.cloneNode(true);
      this.experimentalFloatNextTop.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }
    if (
      node.nodeType == 1 &&
      node.classList.contains(classElemFloatNextBottom)
    ) {
      let clone = node.cloneNode(true);
      this.experimentalFloatNextBottom.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }

    if (
      node.nodeType == 1 &&
      node.classList.contains(classElemFloatSameBottom)
    ) {
      let clone = node.cloneNode(true);
      // this.experimentalFloatNextBottom.push(clone);
      // Remove the element from the flow by hiding it.
      node.style.display = "none";
    }
  }

  beforePageLayout(page, content, breakToken) {
    //console.log(breakToken);
    // If there is an element in the floatPageEls array,
    if (this.experimentalFloatNextTop.length >= 1) {
      // Put the first element on the page.
      page.element
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement("afterbegin", this.experimentalFloatNextTop[0]);
      this.experimentalFloatNextTop.shift();
    }
    if (this.experimentalFloatNextBottom.length >= 1) {
      // Put the first element on the page.
      page.element
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement(
          "afterbegin",
          this.experimentalFloatNextBottom[0]
        );
      this.experimentalFloatNextBottom.shift();
    }
  }

  layoutNode(node) {
    if (node.nodeType == 1) {
      if (node.classList.contains(classElemFloatSameTop)) {
        let clone = node.cloneNode(true);
        clone.classList.add("figadded");
        document
          .querySelector(".pagedjs_pages")
          .lastElementChild.querySelector("article")
          .insertAdjacentElement("afterbegin", clone);
        node.style.display = "none";
        node.classList.add("hide");
      }

      if (
        node.previousElementSibling?.classList.contains(classElemFloatSameTop)
      ) {
        let img = document
          .querySelector(".pagedjs_pages")
          .lastElementChild.querySelector(`.${classElemFloatSameTop}`);
        // console.log(img)
        // console.log(node)
        // if (img?.clientHeight) {
        //   // count the number of line for the image
        //   let imgHeightLine = Math.floor(img.clientHeight / this.baseline)
        //   // add one light and get the height in pixeol
        //   img.dataset.lineOffset = imgHeightLine + 0
        //   img.style.height = `${(imgHeightLine + 0) * this.baseline}px`
        // }
      }
    }
  }
  // works only with non breaked elements
  afterPageLayout(page, content, breakToken) {
    // try fixed bottom on same if requested
    if (page.querySelector("." + classElemFloatSameBottom)) {
      var bloc = page.querySelector("." + classElemFloatSameBottom);
      bloc.classList.add("absolute-bottom");
      bloc.classList.add("figadded");
    }

    // try fixed bottom if requested
    if (page.querySelector("." + classElemFloatNextBottom)) {
      var bloc = page.querySelector("." + classElemFloatNextBottom);
      bloc.classList.add("absolute-bottom");
      bloc.classList.add("figadded");
    }
  }
}
Paged.registerHandlers(elemFloatTop);

/* url cut*/

class urlcut extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  beforeParsed(content) {
    //add wbr to / in links
    // transformed to add ZERO WIDTH SPACE (break opportunity) &#x200B;
    const links = content.querySelectorAll("a");
    links.forEach((link) => {
      if (!link.textContent.includes("http")) {
        return;
      }
      // Rerun to avoid large spaces. Break after a colon or a double slash (//) or before a single slash (/), a tilde (~), a period, a comma, a hyphen, an underline (_), a question mark, a number sign, or a percent symbol.
      const content = link.textContent;
      let printableUrl = content.replace(/\/\//g, "//\u200B");
      // put wbr around everything.
      //printableUrl = printableUrl.replace(/(\/|\~|\-|\=|\,|\_|\?|\#|\&|\;|\%)/g, "$1\u003Cwbr\u003E");
      printableUrl = printableUrl.replace(
        /(\/|\~|\-|\=|\,|\_|\?|\#|\&|\;|\%)/g,
        "$1\u003Cwbr\u003E"
      );
      printableUrl = printableUrl.replace(/\./g, ".\u200B");
      link.setAttribute("data-print-url", printableUrl);
      link.innerHTML = printableUrl;
    });
  }
}
// Paged.registerHandlers(urlcut)

class expMerge extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.experimentalMerged = [];
  }

  onDeclaration(declaration, dItem, dList, rule) {
    // alter the image
    //experimental merge
    if (declaration.property == "--experimental-merge") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.experimentalMerged.push([elId, declaration.value.value]);
      });
    }
  }

  beforeParsed(parsed) {
    if (this.experimentalMerged.length > 0) {
      this.experimentalMerged.forEach((couple) => {
        const host = parsed.querySelector(couple[0]);
        const guest = parsed.querySelector(couple[1]);
        if (!host || !guest) {
          return;
        }
        guest.style.display = "none";
        host.classList.add("merged!");
        host.dataset.mergedGuest = guest.id;
        host.insertAdjacentHTML("beforeend", guest.innerHTML);
      });
    }
  }
}

Paged.registerHandlers(expMerge);

class moveToParentFig extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.moveToParentFig = [];
  }
  onDeclaration(declaration, dItem, dList, rule) {
    // move the element to the next bit
    if (declaration.property == "--experimental-moveToOutsideFigure") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      let itemsList = sel.split(",");
      itemsList.forEach((elId) => {
        this.moveToParentFig.push([elId, declaration.value.value]);
      });
    }
  }
  beforeParsed(content) {
    if (this.moveToParentFig.length > 0) {
      this.moveToParentFig.forEach((elToPush) => {
        const elem = content.querySelector(elToPush[0]);
        if (!elem) {
          return;
        }
        let fighead = elem.querySelector("label").cloneNode(true);
        elem.insertAdjacentElement("beforeend", fighead);
        elem.closest("figure").insertAdjacentElement("afterend", elem);
      });
    }
  }
}
Paged.registerHandlers(moveToParentFig);

function formatUrl(url) {
  // Split the URL into an array to distinguish double slashes from single slashes
  var doubleSlash = url.split("//");

  // Format the strings on either side of double slashes separately
  var formatted = doubleSlash
    .map(
      (str) =>
        // Insert a word break opportunity after a colon
        str
          .replace(/(?<after>:)/giu, "$1<wbr>")
          // Before a single slash, tilde, period, comma, hyphen, underline, question mark, number sign, or percent symbol
          .replace(/(?<before>[/~.,\-_?#%])/giu, "<wbr>$1")
          // Before and after an equals sign or ampersand
          .replace(/(?<beforeAndAfter>[=&])/giu, "<wbr>$1<wbr>")
      // Reconnect the strings with word break opportunities after double slashes
    )
    .join("//<wbr>");

  return formatted;
}

class fixMissingLine extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.moveToParentFig = [];
  }
  afterRendered(pages) {
    document
      .querySelectorAll(".pagedjs_sheet")
      .forEach((sheet) =>
        sheet.style.setProperty("--pagedjs-footnotes-height", "0px")
      );
  }
}
Paged.registerHandlers(fixMissingLine);

// check if the element has a break after avoid and move it on next page
//
class avoidBreakAfter extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterPageLayout(pageFragment, page, breakToken) {
    // get the latest element
    // check if there is a element with break after avoid
    // console.log(page)
    // debugger

    const elementToKeep = [
      ...page.area.querySelectorAll(`[data-break-after=avoid]`),
    ].filter((element) => {
      if (element.nextElementSibling) {
        // console.log(element);
        return;
      }
      let breakTokenPoint = getFirstOf(element);
      return breakTokenPoint;
    });

    let breakTokenPoint = elementToKeep[0];

    if (!breakTokenPoint) return;

    if (breakTokenPoint) {
      // page.element.style.backgroundColor = "red"
    }

    const elementFromSource = this.chunker.source.querySelector(
      `[data-ref="${breakTokenPoint.dataset.ref}"]`
    );

    // console.log(console.log(breakToken));

    if (breakToken) {
      breakToken.node = elementFromSource;
      breakToken.offset = 0;
    }

    // remove the oldest
    // console.log(breakTokenPoint);
    while (breakTokenPoint.nextElementSibling) {
      breakTokenPoint.nextElementSibling.remove();
    }
    breakTokenPoint.remove();
  }
}
Paged.registerHandlers(avoidBreakAfter);

function getFirstOf(element) {
  let firstElement = element;
  while (firstElement && firstElement.dataset.breakAfter == "avoid") {
    // console.log(firstElement);
    firstElement = firstElement.previousElementSibling;
  }

  return firstElement;
}

/** This is a rough draft */
async function formulaeTest(parsed) {
  const imagePromises = [];
  const paraImages = parsed.querySelectorAll("p img");
  paraImages.forEach((image) => {
    const img = new Image();
    let resolve, reject;
    const imageLoaded = new Promise(function(r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function() {
      const { widthClass, heightClass, ratioClass } = getSizeRatioClass(
        img.naturalWidth,
        img.naturalHeight
      );

      image.classList.add(widthClass, heightClass, ratioClass);

      const para = image.closest("p");

      for (child of para?.childNodes) {
        if (child.nodeType === Node.ELEMENT_NODE)
          para.classList.add(
            child.tagName === "IMG" ? "hasMultipleImages" : "hasOtherElem"
          );
        if (child.nodeType === Node.TEXT_NODE) para.classList.add("hasContent");
        if (para.matches(`p.hasMultipleImages.hasOtherElem.hasContent`)) break;
      }
      console.log("loaded-------");
      para.classList.add("hasImage");
      resolve();
    };
    img.onerror = function() {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  try {
    return await Promise.all(imagePromises);
  } catch (err) {
    console.warn(err);
  }
}

function getSizeRatioClass(width, height) {
  return {
    widthClass: getSizeClass(width, "width"),
    heightClass: getSizeClass(height, "height"),
    ratioClass: getRatioClass(width / height),
  };
}
function getSizeClass(size, paramStr) {
  if (size <= 48) return `${paramStr}-40`;
  if (size <= 80) return `${paramStr}-80`;
  if (size <= 160) return `${paramStr}-160`;
  if (size <= 240) return `${paramStr}-240`;
  if (size <= 360) return `${paramStr}-360`;
  if (size <= 480) return `${paramStr}-480`;
  return `${paramStr}-480-above`;
}

function getRatioClass(ratio) {
  if (ratio >= 1.7) return "landscape";
  if (ratio <= 0.95) return "portrait";
  if (ratio < 1.39 || ratio > 0.95) return "square";
}
