// a plugin to work with a baseline
//
// set --pagedjs-baseline-offset-color to see where things are used
// collapsing margin makes things almost impossible to work with as we’re trying to move things around, which add margins to element by default

// next step is finding hte first offset for any object to move the baseline too


class baselineProcess extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.snapToBaselineClass = "pagedjs-snap-to-baseline";
    this.awaitbreak;
    this.snapToBaseline = [];
  }

  //find from the css the element you wanna have on the grid
  onDeclaration(declaration, dItem, dList, rule) {
    if (declaration.property == "--pagedjs-baseline-snap") {
      let sel = csstree.generate(rule.ruleNode.prelude);
      sel = sel.replace('[data-id="', "#");
      sel = sel.replace('"]', "");
      this.snapToBaseline.push(sel.split(","));
    }
  }

  // one day we’ll convert everything to px
  // beforeParsed(content) {
  //     this.snapToBaseline.forEach((selector) => {
  //       content.querySelectorAll(selector).forEach((el) => {
  //       // el.style.lineHeight = the value from the snap css
  //       // converted to px?
  //       });
  //     });
  // }

  //find from the css the element you wanna have on the baseline
  afterParsed(content) {
    if (("this", this.snapToBaseline)) {
      this.snapToBaseline.forEach((selector) => {
        content.querySelectorAll(selector).forEach((el) => {
          el.classList.add(this.snapToBaselineClass);
        });
      });
    }
  }

  // check the offset value
  renderNode(node) {
    // put all node on base line
    if (
      node &&
      node.nodeType == "1" &&
      node.classList.contains(this.snapToBaselineClass)
    ) {
      this.awaitbreak = true;
      startBaseline(node, 22, 6);
    }
  }
}
function startBaseline(element, baseline, offset) {
  // snap element after specific element on the baseline grid.

  if (element && element.offsetTop) {
    // le nb de pixel entre l’élément et son parent
    const elementOffsetTop = element.offsetTop; // element à x pixel de haut

    // get the current line number
    const currentLine = Math.floor(element.offsetTop / baseline); // element à x pixel de haut

    // check the difference between the baseline place and the top offset
    const difference = elementOffsetTop % baseline; // element donc sur la ligne y


    // next line
    const nextLine = Math.ceil(element.offsetTop / baseline);

    // nextLine
    const nextLineOffset = nextLine * baseline;

    // console.log(element);
    // console.log("element offset top", elementOffsetTop);
    // console.log("line", currentLine);
    // console.log("nextline", nextLine);
    // console.log("nextline offset top", nextLineOffset);
    // console.log("difference", difference);
    // console.log("b - d", baseline - difference);

    let mpb, mct;

    if (element.previousElementSibling) {
      mbp = parseInt(getComputedStyle(element.previousElementSibling).marginBottom || 0)
    } else {
      mbp = 0;
    }
    mct = parseInt(getComputedStyle(element).marginTop);

    let min = Math.min(mbp, mct);

    console.log(min)
    // mpb = getComputedStyle(element.previousElementSibling).marginTop

    if (difference > 0) {
      //   // to debug, uncomment those lines
      //   console.log(element);
      //   console.log("elementoffset", elementOffset);
      //   console.log("diff", difference);
      //   console.log("nextLineOffset", nextLineOffset);
      //   console.log("baseline - diff", baseline - difference);
      //   // console.log("offset", offset);
      //

      // element margin top must be changed to get the right amoutn

//       element.insertAdjacentHTML(
//         "beforebegin",
//         `<div class="pagedjs-offset-baseline" style="margin-top:${}px;
// height:${baseline - difference
//         }px; background: var(--pagedjs-baseline-offset-color); "></div>`
//       );
      element.style.paddingTop = baseline - difference + "px";
    }
  }
}

Paged.registerHandlers(baselineProcess);

// function getTheBigMargins(element) {
//   // getMarginTop of our element:
//   const marginCurrent = parseInt(getComputedStyle(element).marginTop);
//
//   if (element.previousElementSibling) {
//     const marginPrevious = getComputedStyle(element.previousElementSibling)
//       .marginBottom
//       ? parseInt(getComputedStyle(element.previousElementSibling).marginBottom)
//       : 0;
//   } else {
//     marginPrevious = 0;
//   }
//
//   return Math.max(marginPrevious ? marginPrevious : 0, marginCurrent) -
//     Math.min(marginPrevious ? marginPrevious : 0, marginCurrent);
// }
//
//
//
// margin things:
//
//
//
// let previousMarginBottomOffset;
// // get the previous margin bottom
// if (element.previousElementSibling) {
//   previousMarginBottomOffset = parseInt(
//     getComputedStyle(element.previousElementSibling).marginBottom
//   );
// } else {
//   previousMarginBottomOffset = 0;
// }
//
// // get the current margin top
// currentMarginTopOffset = parseInt(getComputedStyle(element).marginTop);
//
// console.log("mto", currentMarginTopOffset)
// console.log("mbo", previousMarginBottomOffset)
// // console.log("mto", currentMa)
//
// let marginOffst = Math.max(
//   previousMarginBottomOffset,
//   currentMarginTopOffset
// );
//
//
//
//
//
//

function polyfillCollapsingMargins(element1, element2) {
  // Check if elements exist
  if (!element1 || !element2) {
    return { newMarginTop: 0, newMarginBottom: 0 };
  }

  // Get computed margins
  const style1 = getComputedStyle(element1);
  const style2 = getComputedStyle(element2);

  // Calculate new margins
  const marginTop1 = parseInt(style1.marginTop);
  const marginBottom1 = parseInt(style1.marginBottom);
  const marginTop2 = parseInt(style2.marginTop);
  const marginBottom2 = parseInt(style2.marginBottom);

  // Adjust margins
  const newMarginBottom1 = Math.max(marginBottom1, marginTop2) || 0;
  const newMarginTop2 = Math.max(marginTop2, marginTop1) || 0;

  return { newMarginTop: newMarginTop2, newMarginBottom: newMarginBottom1 };
}
